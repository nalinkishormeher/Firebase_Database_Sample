package com.ghigared.example.firebasedatabasesampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DataAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        addChildEventListener();
    }

    private void initRecyclerView(){
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        adapter = new DataAdapter();

        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void addChildEventListener() {
        FirebaseDatabase.getInstance().getReference("posts").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.addElement(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.removeElement(dataSnapshot);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder>{
        public ArrayList<DataSnapshot> dataList;

        public DataAdapter(){
            dataList = new ArrayList<>();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(getLayoutInflater().inflate(R.layout.recycler_view_row, null));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.title.setText(dataList.get(position).child("title").getValue().toString());
            holder.body.setText(dataList.get(position).child("body").getValue().toString());
        }

        public void addElement(DataSnapshot dataSnapshot){
            dataList.add(dataSnapshot);
            notifyDataSetChanged();
        }

        public void removeElement(DataSnapshot dataSnapshot){
            dataList.remove(dataSnapshot);
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public TextView title;
            public TextView body;

            public ViewHolder(View v){
                super(v);
                title = (TextView) v.findViewById(R.id.title);
                body = (TextView   ) v.findViewById(R.id.body);
            }
        }
    }
}
